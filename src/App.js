import ProductList from "./components/ProductList";
import { CatalogueProvider } from "./Providers/catalogue";
import { CartProvider } from "./Providers/cart";
import GlobalStyle from "./styles/global";

function App() {
  return (
    <>
      <GlobalStyle />
      <CatalogueProvider>
        <CartProvider>
          <ProductList type="catalogue" />
          <ProductList type="cart" />
        </CartProvider>
      </CatalogueProvider>
    </>
  );
}

export default App;
