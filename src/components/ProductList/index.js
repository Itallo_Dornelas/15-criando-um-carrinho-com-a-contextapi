import Button from "../Button";

import { useContext } from "react";

import { CatalogueContext } from "../../Providers/catalogue";
import { CartContext } from "../../Providers/cart";

import { Container, List } from "./styles";

const ProductList = ({ type }) => {
  const { catalogue } = useContext(CatalogueContext);
  const { cart } = useContext(CartContext);

  return (
    <>
      <Container>
        <ul>
          <div>
            {type === "catalogue"
              ? catalogue.map((item, index) => (
                  <List key={index}>
                    {item.name}
                    <Button type={type} item={item} />
                  </List>
                ))
              : cart.map((item, index) => (
                  <List key={index}>
                    {item.name}
                    <Button type={type} item={item} />
                  </List>
                ))}
          </div>
        </ul>
      </Container>
    </>
  );
};

export default ProductList;
