import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 10%;
  align-items: center;
  justify-content: center;
  div {
    display: flex;
    flex-direction: column;
    border: 2px solid #c85311;
    justify-content: center;
  }
  button {
    margin-top: 5px;
    background: #000;
    color: #c85311;
    border: 0;
    border-radius: 40px 40px 40px 40px;
    display: flex;
    align-items: center;
    transition: 200ms ease-in-out;
    padding: 5px;
    font-size: 1.5rem;
    flex: 1;
    text-align: center;
    font-weight: bold;

    :hover {
      background-color: #224959;
    }
  }
`;

export const List = styled.li`
  padding: 5px;
  font-size: 1.5rem;
  border: none;
  color: #224959;
  list-style: none;
`;
