import { useContext } from "react";

import { CartContext } from "../../Providers/cart";
import { CatalogueContext } from "../../Providers/catalogue";

const Button = ({ type, item }) => {
  const { addToCart, removeToCart } = useContext(CartContext);
  const { catalogue, addToCatalogue, removeToCatalogue } =
    useContext(CatalogueContext);

  const text = type === "catalogue" ? "Add to cart" : "Remove from cart";

  const handleClick = () => {
    if (type === "catalogue") {
      removeToCatalogue(item);
      addToCart(item);
    } else {
      removeToCart(item);
      addToCatalogue(item);
    }
  };

  return (
    <div>{catalogue && <button onClick={handleClick}>{text}</button>}</div>
  );
};

export default Button;
